# Uninstall old version
# sudo rm /usr/local/bin/docker-compose


# Install Docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose


# Change mode execute
sudo chmod +x /usr/local/bin/docker-compose


# Add user to docker group use docker command without sudo
# sudo usermod -a -G docker $(whoami)
