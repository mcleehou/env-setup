sudo rm -rf /usr/local/go

GOVERSION="1.17.3"

wget "https://golang.org/dl/go${GOVERSION}.linux-amd64.tar.gz" -4
sudo tar -C /usr/local -xvf "go${GOVERSION}.linux-amd64.tar.gz"

cat >> ~/.bashrc << 'EOF'
export GOPATH=$HOME/go
export PATH=/usr/local/go/bin:$PATH:$GOPATH/bin
EOF

source ~/.bashrc
cat >> ~/.zshrc << 'EOF'
export GOPATH=$HOME/go
export PATH=/usr/local/go/bin:$PATH:$GOPATH/bin
EOF

source ~/.zshrc

